<?php

use App\Http\Controllers\AirlineController;
use App\Http\Controllers\AirportController;
use App\Http\Controllers\CountryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect()->route('airports');
});

Route::get('/countries', [CountryController::class, 'index'])->name('countries');
Route::get('/countries/new', [CountryController::class, 'new'])->name('countries.new');
Route::post('/countries/new', [CountryController::class, 'create']);
Route::get('/countries/{country}/edit', [CountryController::class, 'edit'])->name('countries.edit');
Route::post('/countries/{country}/edit', [CountryController::class, 'update']);
Route::get('/countries/{country}/remove', [CountryController::class, 'remove'])->name('countries.remove');
Route::post('/countries/{country}/remove', [CountryController::class, 'delete']);
Route::get('/countries/withoutAirline', [CountryController::class, 'withoutAirline'])->name('countries.withoutAirline');
Route::get('/countries/withoutAirlineAndAirport', [CountryController::class, 'withoutAirlineAndAirport'])->name('countries.withoutAirlineAndAirport');

Route::get('/airports', [AirportController::class, 'index'])->name('airports');
Route::get('/airports/search', [AirportController::class, 'search'])->name('airports.search');
Route::get('/airports/new', [AirportController::class, 'new'])->name('airports.new');
Route::post('/airports/new', [AirportController::class, 'create']);
Route::get('/airports/{airport}', [AirportController::class, 'view'])->name('airports.view');
Route::get('/airports/{airport}/newAirline', [AirportController::class, 'newAirline'])->name('airports.newAirline');
Route::post('/airports/{airport}/newAirline', [AirportController::class, 'addAirline']);
Route::get('/airports/{airport}/removeAirline', [AirportController::class, 'removeAirline'])->name('airports.removeAirline');
Route::post('/airports/{airport}/removeAirline', [AirportController::class, 'deleteAirline']);
Route::get('/airports/{airport}/edit', [AirportController::class, 'edit'])->name('airports.edit');
Route::post('/airports/{airport}/edit', [AirportController::class, 'update']);
Route::get('/airports/{airport}/remove', [AirportController::class, 'remove'])->name('airports.remove');
Route::post('/airports/{airport}/remove', [AirportController::class, 'delete']);

Route::get('/airlines', [AirlineController::class, 'index'])->name('airlines');
Route::get('/airlines/new', [AirlineController::class, 'new'])->name('airlines.new');
Route::post('/airlines/new', [AirlineController::class, 'create']);
Route::get('/airlines/new/{airline}/remove', [AirlineController::class, 'remove'])->name('airlines.remove');
Route::post('/airlines/new/{airline}/remove', [AirlineController::class, 'delete']);
Route::get('/airlines/new/{airline}/edit', [AirlineController::class, 'edit'])->name('airlines.edit');
Route::post('/airlines/new/{airline}/edit', [AirlineController::class, 'update']);
