# PHP Programuotojas - Techninis testas

> Šios užduoties tikslas yra patikrinti Jūsų gebėjimus, kaip Jūs suprantate reikalavimus ir kaip juos realizuojate.
> Galite naudoti komentarus, kur manote, kad jie reikalingi. Atkreipkite dėmesį į kodo formatavimo standartus, geriausias kodo rašymo praktikas, kodo bei projekto struktūrą.
> 
>Sėkmės!

# Užduotis

Sukurti įrašymo/skaitymo/atnaujinimo/trynimo (CRUD) web aplikaciją oro uostų valdymui.
Reikalavimai duomenų struktūrai:

Kiekvienas oro uostas turi:

- Pavadinimą
- Šalį
- Lokaciją (ilguma ir platuma)
- Vieną ar daugiau susietų avialinijų (kokios avialinijos leidžiasi ir kyla iš oro uosto)


Kiekviena šalis turi:

- Šalies kodą (ISO)
- Šalies pavadinimą

Kiekviena avialinija turi:

- Pavadinimą
- Šalį

Oro uostai bei avialinijos turėtų būti valdomos naudojantis sukurtu įrankiu. Šalys gali būti statiškai nurodomos naudojantis duomenų baze. Šalių kodus bei pačias šalis galite rasti [https://countrycode.org/](https://countrycode.org/) puslapyje (visų suvedinėti nereikia, panaudokite bent keletą). 
Oro uosto koordinačių parinkimas turėtų veikti naudojantis interaktyviu žemėlapiu. Paspaudus žemėlapyje lokaciją būtų nustatomos oro uosto koordinates. Galima naudoti Google Maps. Atkreipkite dėmesį į sukurtos sistemos saugumą ir kaip apsaugoti vartotoją nuo netinkamų duomenų talpinimo.

Naudojantis web aplikacija turi būti galimybė pasiekti visus sukurtus puslapius.

Web aplikacijai sukurti reiktų naudoti PHP + MySQL technologijas. 
Sistemai sukurti `NENAUDOKITE` PHP framework(Symfony, Laravel, CodeIgniter, Zend ar kito), taip norime įvertinti Jūsų patirtį ir įgūdžius. 
Jei be framework negalite išsiversti tuomet panaudokite Jums žinomą PHP framework ar bibliotekas kurios Jums padės atlikti užduotį. 
Pabaigta užduotis turėtų atspindėti kaip Jūs suprantate funkcinius reikalavimus bei parodyti Jūsų technologines žinias.

Web aplikacijos išvaizdai nereikalaujama rimtų UI/UX sprendimų. Galite panaudoti Bootstrap ar kitą karkasą/biblioteką.

Atliktą užduotį patalpinkite pasirinktoje svetainių talpykloje(kaip pvz. nemokama paslaugą teikia [https://www.000webhost.com/](https://www.000webhost.com/)
Užduoties kodą(source) galite atsiųsti ZIP failu arba patalpinti pvz. [https://bitbucket.org/](https://bitbucket.org/)

# Papildoma užduotis

> Įvertinsime ir papildomai atliktą užduotį

Parašykite kelis punktus kaip projektas galėtų būti plėtojamas. Pvz. galima pridėti avialinijų/lėktuvų atvykimo, išvykimo laikus.
Padarykite ataskaitas:

- Šalių sąrašas kurioms nėra priskirtos avialinijos
- Šalių sąrašas kurios neturi avialinijų ir oro uostų
- Oro uostai į kuriuos skrenda tik pasirinktos šalies avialinijos

