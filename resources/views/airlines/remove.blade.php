@extends('layouts.app')

@section('content')
  <div class="text-center">
    <h1>Are you sure you want to delete {{$airline->name}}, {{\App\Models\Country::where('id', $airline->country_id)->first()->name}} airline?</h1>
    <p>This action will remove all associations with airports</p>
  </div>
  <hr>
  <form action="" method="post" class="text-center">
    @csrf
    <div class="btn-group" role="group">
      <button type="submit" class="btn btn-danger btn-lg">Yes</button>
      <a href="{{route('airlines')}}" class="btn btn-success btn-lg">No</a>
    </div>
  </form>
@endsection