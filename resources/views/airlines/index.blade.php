@extends('layouts.app')

@section('content')
  <div class="text-center">
    <a href="{{route('airlines.new')}}" class="btn btn-danger">New airline</a>
  </div>
  <hr>
  <table class="table table-dark table-striped">
    <thead>
      <tr>
        <th scope="col">Name</th>
        <th scope="col">Country</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($airlines as $airline)
      <tr>
        <th>{{$airline->name}}</th>
        <td>{{\App\Models\Country::where('id', $airline->country_id)->first()->name}}</td>
        <td>
          <a href="{{route('airlines.edit', ['airline' => $airline->id])}}" class="btn btn-warning">Edit</a>
          <a href="{{route('airlines.remove', ['airline' => $airline->id])}}" class="btn btn-danger">Delete</a>
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
@endsection