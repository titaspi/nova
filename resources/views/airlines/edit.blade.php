@extends('layouts.app')

@section('content')
<div class="text-center">
  <h1>Let's edit {{$airline->name}} airline!</h1>
</div>
<hr>
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<form action="" method="post">
  @csrf
  <div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{$airline->name}}">
  </div>
  <div class="mb-3">
    <select class="form-select @error('country') is-invalid @enderror" aria-label="Select country" id="country" name="country">
      <option>Select country</option>
      @foreach (\App\Models\Country::all() as $country)
      <option value="{{$country->id}}" @if($airline->country_id == $country->id) selected @endif>{{$country->name}}</option>
      @endforeach
    </select>
  </div>
  <button type="submit" class="btn btn-success">Edit</button>
</form>

@endsection
