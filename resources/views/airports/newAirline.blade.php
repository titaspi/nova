@extends('layouts.app')

@section('content')
<div class="text-center">
  <h1>Let's add airline to {{$airport->name}} airport!</h1>
</div>
<hr>
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<form action="" method="post">
  @csrf
  <div class="mb-3">
    <select class="form-select @error('airline') is-invalid @enderror" aria-label="Select airline" id="airline" name="airline">
      <option selected>Select airline</option>
      @foreach ($airlines as $airline)
      <option value="{{$airline->id}}">{{$airline->name}} | {{\App\Models\Country::where('id', $airline->country_id)->first()->name}}</option>
      @endforeach
    </select>
  </div>
  <button type="submit" class="btn btn-success">Link</button>
</form>

@endsection
