@extends('layouts.app')

@section('content')
  <div class="text-center">
    <h1>Are you sure you want to delete {{$airport->name}}, {{\App\Models\Country::where('id', $airport->country_id)->first()->name}} airport?</h1>
    <p>This action will remove all associations with other airlines and countries</p>
  </div>
  <hr>
  <form action="" method="post" class="text-center">
    @csrf
    <div class="btn-group" role="group">
      <button type="submit" class="btn btn-danger btn-lg">Yes</button>
      <a href="{{route('airports')}}" class="btn btn-success btn-lg">No</a>
    </div>
  </form>
@endsection