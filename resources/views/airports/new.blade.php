@extends('layouts.app')

@section('content')
<script src='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css' rel='stylesheet' />
<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v5.0.0/mapbox-gl-geocoder.min.js"></script>
<link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v5.0.0/mapbox-gl-geocoder.css" type="text/css">

<div class="text-center">
  <h1>Let's create a new airport!</h1>
</div>
<hr>
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<form action="" method="post">
  @csrf
  <div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{old('name')}}">
  </div>
  <div class="mb-3">
    <select class="form-select @error('country') is-invalid @enderror" aria-label="Select country" id="country" name="country">
      <option selected>Select country</option>
      @foreach (\App\Models\Country::all() as $country)
      <option value="{{$country->id}}">{{$country->name}}</option>
      @endforeach
    </select>
  </div>
  <div class="mb-3">
    <label for="location" class="form-label">Location</label>
    <input type="text" class="form-control @error('location') is-invalid @enderror" id="location" name="location" value="{{old('location')}}" readonly>
    <div id='map' style='width: 100%; height: 500px;'></div>
  </div>
  <button type="submit" class="btn btn-success">Create</button>
</form>
<script>
  const locationElement = document.getElementById('location');
  mapboxgl.accessToken = 'pk.eyJ1IjoidGl0YXNueGx0IiwiYSI6ImNqZWs3ZHliejBjOWMzM284aG1nbG1yN3IifQ._nFPiSI4HSaZriIEDwRa8g';
  var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    doubleClickZoom: false
  });

  // Add the control to the map.
  map.addControl(
    new MapboxGeocoder({
      accessToken: mapboxgl.accessToken,
      mapboxgl: mapboxgl
    })
  );

  let marker = new mapboxgl.Marker()
    .setLngLat([30.5, 50.5])
    .addTo(map);

  map.on('dblclick', (e) => {
    marker.setLngLat(e.lngLat);
    locationElement.value = `${e.lngLat.lng} ${e.lngLat.lat}`;
  })

</script>

@endsection
