@extends('layouts.app')

@section('content')
<script src='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css' rel='stylesheet' />
<div class="text-center">
  <h1>{{$airport->name}}</h1>
</div>
<hr>
<div class="row">
  <div class="col text-center">
    <div class="card mx-auto" style="width: 50%;">
      <div class="card-body">
        <h5 class="card-title">{{$airport->name}}</h5>
        <p class="card-text">
          Comfy airport located at {{\App\Models\Country::where('id', $airport->country_id)->first()->name}}<br />
          List of airlines flying to/from {{$airport->name}}:<br />
          {{$airport->Airlines->implode('name', ', ')}}
        </p>
      </div>
    </div>
  </div>
  <div class="col">
    <div id="map" style='width: 100%; height: 500px;'></div>
  </div>
</div>

<script>
  const locationElement = "{{$airport->location}}";
  mapboxgl.accessToken = 'pk.eyJ1IjoidGl0YXNueGx0IiwiYSI6ImNqZWs3ZHliejBjOWMzM284aG1nbG1yN3IifQ._nFPiSI4HSaZriIEDwRa8g';
  var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    doubleClickZoom: false,
    center: locationElement.split(' '),
    zoom: 7
  });

  let marker = new mapboxgl.Marker()
    .setLngLat(locationElement.split(' '))
    .addTo(map);

</script>
@endsection
