@extends('layouts.app')

@section('content')
  <div class="text-center">
    <a href="{{route('airports.new')}}" class="btn btn-danger">New airport</a><br />
    <form action="{{route('airports.search')}}" method="get">
      <div class="mb-3">
        <select class="form-select" aria-label="Search by country" id="country" name="country">
          <option selected>Search by country</option>
          @foreach (\App\Models\Country::all() as $country)
          <option value="{{$country->id}}">{{$country->name}}</option>
          @endforeach
        </select>
        <button type="submit" class="btn btn-success">Search</button>
      </div>
    </form>
  </div>
  <hr>
  <table class="table table-dark table-striped">
    <thead>
      <tr>
        <th scope="col">Name</th>
        <th scope="col">Country</th>
        <th scope="col">Location</th>
        <th scope="col">Airlines</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($airports as $airport)
      <tr>
        <th><a href="{{route('airports.view', ['airport' => $airport->id])}}">{{$airport->name}}</a></th>
        <td>{{\App\Models\Country::where('id', $airport->country_id)->first()->name}}</td>
        <td>{{$airport->location}}</td>
        <td>
          {{$airport->Airlines->implode('name', ', ')}}
          {{-- @foreach ($airport->Airlines as $airline)
            {{$airline->name}}
          @endforeach --}}
        </td>
        <td>
          <a href="{{route('airports.newAirline', ['airport' => $airport->id])}}" class="btn btn-success">Add airline</a>
          <a href="{{route('airports.removeAirline', ['airport' => $airport->id])}}" class="btn btn-danger">Remove airline</a>
          <a href="{{route('airports.edit', ['airport' => $airport->id])}}" class="btn btn-warning">Edit</a>
          <a href="{{route('airports.remove', ['airport' => $airport->id])}}" class="btn btn-danger">Delete</a>
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
@endsection