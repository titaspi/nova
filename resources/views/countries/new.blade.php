@extends('layouts.app')

@section('content')
<div class="text-center">
  <h1>Let's create a new country!</h1>
</div>
<hr>
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<form action="" method="post">
  @csrf
  <div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Lithuania" value="{{old('name')}}">
  </div>
  <div class="mb-3">
    <label for="iso_code" class="form-label">ISO Code</label>
    <input type="text" class="form-control @error('iso_code') is-invalid @enderror" id="iso_code" name="iso_code" value="{{old('iso_code')}}">
  </div>
  <button type="submit" class="btn btn-success">Create</button>
</form>
@endsection
