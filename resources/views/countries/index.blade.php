@extends('layouts.app')

@section('content')
  <div class="text-center">
    <a href="{{route('countries.new')}}" class="btn btn-danger">New country</a>
    <a href="{{route('countries.withoutAirline')}}" class="btn btn-danger">Countries without airlines</a>
    <a href="{{route('countries.withoutAirlineAndAirport')}}" class="btn btn-danger">Countries without airlines and airports</a>
  </div>
  <hr>
  <table class="table table-dark table-striped">
    <thead>
      <tr>
        <th scope="col">Name</th>
        <th scope="col">Code</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($countries as $country)
        <tr>
          <td>{{$country->name}}</td>
          <td>{{$country->iso_code}}</td>
          <td>
            <a href="{{route('countries.edit', ['country' => $country->id])}}" class="btn btn-warning">Edit</a>
            <a href="{{route('countries.remove', ['country' => $country->id])}}" class="btn btn-danger">Delete</a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection