@extends('layouts.app')

@section('content')
  <div class="text-center">
    <h1>Are you sure you want to delete {{$country->name}} country?</h1>
    <p>This action will remove all other airlines and countries associated with this country.</p>
  </div>
  <hr>
  <form action="" method="post" class="text-center">
    @csrf
    <div class="btn-group" role="group">
      <button type="submit" class="btn btn-danger btn-lg">Yes</button>
      <a href="{{route('countries')}}" class="btn btn-success btn-lg">No</a>
    </div>
  </form>
@endsection