<nav class="navbar navbar-dark bg-dark navbar-expand-lg">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">{{ config('app.name', 'Laravel') }}</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="{{ route('airports') }}">Airports</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('countries') }}">Countries</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ route('airlines') }}">Airlines</a>
        </li>
      </ul>
    </div>
  </div>
</nav>