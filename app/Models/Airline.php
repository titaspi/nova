<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Airline extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'country_id'
    ];

    public function Airports() {
        return $this->belongsToMany(Airport::class, 'airport_airlines', 'airline_id', 'airport_id');
    }
}
