<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'country_id',
        'location'
    ];

    public function Airlines() {
        return $this->belongsToMany(Airline::class, 'airport_airlines', 'airport_id', 'airline_id');
    }
}
