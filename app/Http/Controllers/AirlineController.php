<?php

namespace App\Http\Controllers;

use App\Models\Airline;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AirlineController extends Controller
{
    public function index()
    {
        $airlines = Airline::all();

        return view('airlines.index', compact('airlines'));
    }

    public function new()
    {
        return view('airlines.new');
    }

    public function create(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string|unique:airports|max:255',
            'country' => 'required|exists:countries,id'
        ]);

        Airline::create([
            'name' => request('name'),
            'country_id' => request('country')
        ]);

        return redirect()->route('airlines');
    }

    public function edit(Airline $airline)
    {
        return view('airlines.edit', compact('airline'));
    }

    public function update(Airline $airline, Request $request)
    {
        $validated = $request->validate([
            'name' => [
                'required', 'string',
                Rule::unique('airlines')->ignore($airline->id, 'id'),
                'max:255'],
            'country' => 'required|exists:countries,id'
        ]);

        $airline->update([
            'name' => request('name'),
            'country_id' => request('country')
        ]);

        return redirect()->route('airlines');
    }

    public function remove(Airline $airline)
    {
        return view('airlines.remove', compact('airline'));
    }

    public function delete(Airline $airline)
    {
        $airline->Airports()->detach();
        $airline->delete();

        return redirect()->route('airlines');
    }
}
