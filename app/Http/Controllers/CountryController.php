<?php

namespace App\Http\Controllers;

use App\Models\Airline;
use App\Models\Airport;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CountryController extends Controller
{
    public function index()
    {
        $countries = Country::all();

        return view('countries.index', compact('countries'));
    }
    public function withoutAirline()
    {
        // Poor efficiency. Need to create proper relationships to optimize it.
        $airlines = Airline::all();
        
        $countries = [];
        
        foreach (Country::all() as $country) {
            if (!$airlines->contains('country_id', $country->id)) $countries[] = $country;
        }
        
        return view('countries.index', compact('countries'));
    }
    public function withoutAirlineAndAirport()
    {
        // Poor efficiency. Need to create proper relationships to optimize it.
        $airlines = Airline::all();
        $airports = Airport::all();
        
        $countries = [];
        
        foreach (Country::all() as $country) {
            if (!$airlines->contains('country_id', $country->id) && !$airports->contains('country_id', $country->id)) $countries[] = $country;
        }
        
        return view('countries.index', compact('countries'));
    }

    public function new()
    {
        return view('countries.new');
    }

    public function create(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string|unique:countries|max:255',
            'iso_code' => 'required|string'
        ]);

        Country::create($validated);

        return redirect()->route('countries');
    }

    public function edit(Country $country)
    {
        return view('countries.edit', compact('country'));
    }

    public function update(Country $country, Request $request)
    {
        $validated = $request->validate([
            'name' => [
                'required', 'string',
                Rule::unique('countries')->ignore($country->id, 'id'),
                'max:255'],
            'iso_code' => 'required|string'
        ]);

        $country->update([
            'name' => request('name'),
            'iso_code' => request('iso_code')
        ]);

        return redirect()->route('countries');
    }

    public function remove(Country $country)
    {
        return view('countries.remove', compact('country'));
    }

    public function delete(Country $country)
    {
        Airline::where('country_id', $country->id)->delete();
        Airport::where('country_id', $country->id)->delete();
        $country->delete();

        return redirect()->route('countries');
    }
}
