<?php

namespace App\Http\Controllers;

use App\Models\Airline;
use App\Models\Airport;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AirportController extends Controller
{
    public function index()
    {
        $airports = Airport::all();

        return view('airports.index', compact('airports'));
    }

    public function new()
    {
        return view('airports.new');
    }

    public function view(Airport $airport)
    {
        return view('airports.view', compact('airport'));
    }

    public function search(Request $request)
    {
        if (isset($_GET['country'])) {
            $airports = Airport::where('country_id', $_GET['country'])->get();
        }
        else {
            $airports = Airport::all();
        }
        return view('airports.index', compact('airports'));
    }

    public function create(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string|unique:airports|max:255',
            'country' => 'required|exists:countries,id',
            'location' => 'required'
        ]);

        Airport::create([
            'name' => request('name'),
            'country_id' => request('country'),
            'location' => request('location')
        ]);

        return redirect()->route('airports');
    }

    public function newAirline(Airport $airport)
    {
        // except($airport->Airlines()->get(['airline_id']));
        $airlines = Airline::all();
        return view('airports.newAirline', compact('airport', 'airlines'));
    }

    public function addAirline(Airport $airport, Request $request)
    {
        if ($airport->Airlines->where('id', request('airline'))->count() == 0) {
            $airport->Airlines()->attach(request('airline'));
        }

        return redirect()->route('airports');
    }
    
    public function removeAirline(Airport $airport)
    {
        return view('airports.removeAirline', compact('airport'));
    }

    public function deleteAirline(Airport $airport, Request $request)
    {
        if ($airport->Airlines->where('id', request('airline'))->count() > 0) {
            $airport->Airlines()->detach(request('airline'));
        }
        
        return redirect()->route('airports');
    }

    public function edit(Airport $airport)
    {
        return view('airports.edit', compact('airport'));
    }

    public function update(Airport $airport, Request $request)
    {
        $validated = $request->validate([
            'name' => [
                'required', 'string',
                Rule::unique('airports')->ignore($airport->id, 'id'),
                'max:255'],
            'country' => 'required|exists:countries,id',
            'location' => 'required'
        ]);

        $airport->update([
            'name' => request('name'),
            'country_id' => request('country'),
            'location' => request('location')
        ]);

        return redirect()->route('airports');
    }

    public function remove(Airport $airport)
    {
        return view('airports.remove', compact('airport'));
    }

    public function delete(Airport $airport)
    {
        $airport->Airlines()->detach();
        $airport->delete();

        return redirect()->route('airports');
    }
}
